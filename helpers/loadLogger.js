const Load = require('../models/Load');

const log = async (id, message) => {
  const load = await Load.findById(id);
  load.logs.push({message});
  await Load.findByIdAndUpdate(id, {$set: {logs: load.logs}});
};

module.exports = {log};
