const Joi = require('joi');


const joiPassword = Joi.string().required();

const userRegSchema = Joi.object({
  email: Joi.string()
      .trim()
      .email()
      .required(),

  password: joiPassword,

  role: Joi.string()
      .valid('DRIVER', 'SHIPPER')
      .required(),
});

const credentialsSchema = Joi.object({
  email: Joi.string()
      .trim()
      .email()
      .required(),

  password: joiPassword,
});

const passwordsSchema = Joi.object({
  oldPassword: joiPassword,
  newPassword: joiPassword,
});

module.exports = {credentialsSchema, userRegSchema, passwordsSchema};
