const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const {loadStatuses} = require('../../constants/modelConstants');

const paramSchema = Joi.number()
    .integer()
    .min(0)
    .default(0);

const queryParamsSchema = Joi.object({
  status: Joi.string()
      .valid(...loadStatuses),
  offset: paramSchema,
  limit: paramSchema,
});

const mongoIdSchema = Joi.object({
  id: Joi.objectId()
      .required(),
});

module.exports = {
  queryParamsSchema,
  mongoIdSchema,
};
