const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const {truckTypes} = require('../../constants/modelConstants');

const typeSchema = Joi.object({
  type: Joi.string()
      .valid(...truckTypes)
      .required(),
});

module.exports = {typeSchema};
