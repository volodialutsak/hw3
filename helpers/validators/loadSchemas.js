const Joi = require('joi');

const dimensionsSchema = Joi.object({
  length: Joi.number()
      .positive(0)
      .required(),

  width: Joi.number()
      .positive(0)
      .required(),

  height: Joi.number()
      .positive(0)
      .required(),
});

const addLoadSchema = Joi.object({
  name: Joi.string()
      .trim()
      .required(),

  payload: Joi.number()
      .min(0)
      .required(),

  pickup_address: Joi.string()
      .trim()
      .required(),

  delivery_address: Joi.string()
      .trim()
      .required(),

  dimensions: dimensionsSchema
      .required(),
});

const updateLoadSchema = Joi.object({
  name: Joi.string()
      .trim(),

  payload: Joi.number()
      .min(0),

  pickup_address: Joi.string()
      .trim(),

  delivery_address: Joi.string()
      .trim(),

  dimensions: dimensionsSchema.not({}),
});

module.exports = {addLoadSchema, updateLoadSchema};
