const truckDimensions = require('../constants/truckDimensions');

const checkDimensions = (truckType, {length, width, height}, payload) => {
  const {
    truckLength,
    truckWidth,
    truckHeight,
    truckPayload,
  } = truckDimensions[truckType];

  if (payload > truckPayload) {
    return false;
  }

  if (
    (length <= truckLength && width <= truckWidth && height <= truckHeight) ||
    (length <= truckLength && width <= truckHeight && height <= truckWidth) ||
    (length <= truckWidth && width <= truckHeight && height <= truckLength) ||
    (length <= truckWidth && width <= truckLength && height <= truckHeight) ||
    (length <= truckHeight && width <= truckLength && height <= truckWidth) ||
    (length <= truckHeight && width <= truckWidth && height <= truckLength)
  ) {
    return true;
  }

  return false;
};

module.exports = checkDimensions;
