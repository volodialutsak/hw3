const config = {
  PORT: process.env.PORT || 8080,
  ENV: process.env.NODE_ENV || 'dev',
  JWT_SECRET: process.env.JWT_SECRET || 'topsecret',
  MONGO_URI: process.env.MONGO_URI || 'mongodb+srv://Volodymyr:Volodymyr00@hw3.tzdmd.mongodb.net/HW3?retryWrites=true&w=majority',
};

module.exports = config;
