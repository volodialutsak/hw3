const roles = ['DRIVER', 'SHIPPER'];
const truckStatuses = ['OL', 'IS'];
const truckTypes = [
  'SPRINTER',
  'SMALL STRAIGHT',
  'LARGE STRAIGHT'];
const loadStatuses = [
  'NEW',
  'POSTED',
  'ASSIGNED',
  'SHIPPED',
];
const loadStates = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];


module.exports = {
  roles,
  truckStatuses,
  truckTypes,
  loadStatuses,
  loadStates,
};
