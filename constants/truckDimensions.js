module.exports = {
  'SPRINTER': {
    truckLength: 300,
    truckWidth: 250,
    truckHeight: 170,
    truckPayload: 1700,
  },
  'SMALL STRAIGHT': {
    truckLength: 500,
    truckWidth: 250,
    truckHeight: 170,
    truckPayload: 2500,
  },
  'LARGE STRAIGHT': {
    truckLength: 700,
    truckWidth: 350,
    truckHeight: 200,
    truckPayload: 4000,
  },
};
