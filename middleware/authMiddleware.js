const jwt = require(`jsonwebtoken`);
const bcrypt = require('bcrypt');
const User = require('../models/User');
const asyncHandler = require('express-async-handler');
const ApiError = require(`./utils/ApiError`);
const config = require('../config/config');

const tokenParser = async (req) => {
  const authorization = req.headers.authorization;

  if (!authorization) {
    throw ApiError.badRequest(`Unauthorized request. Missing token`);
  }

  const token = authorization.split(' ')[1];
  const decoded = jwt.verify(token, config.JWT_SECRET);

  const user = await User.findById(decoded._id).select('-__v');
  if (!user) {
    throw ApiError.badRequest(`Unauthorized request. Bad token`);
  }

  const isValidPassword = await bcrypt.compare(decoded.password, user.password);
  if (!isValidPassword) {
    throw ApiError.badRequest(`Unauthorized request. Bad token`);
  }

  req.user = user;
};

const commonAuth = asyncHandler( async (req, res, next) => {
  await tokenParser(req);
  next();
});

const driverAuth = asyncHandler( async (req, res, next) => {
  await tokenParser(req);

  if (req.user.role !== 'DRIVER') {
    throw ApiError.badRequest('Unauthorized request. Bad token');
  }

  next();
});

const shipperAuth = asyncHandler( async (req, res, next) => {
  await tokenParser(req);

  if (req.user.role !== 'SHIPPER') {
    throw ApiError.badRequest('Unauthorized request. Bad token');
  }

  next();
});

module.exports = {commonAuth, driverAuth, shipperAuth};
