
/**
 * Class for generating custom comments
  */
class ApiError {
  /**
  * @param {int} code
  * @param {string} message
  */
  constructor(code, message) {
    this.code = code;
    this.message = message;
  }
  /**
  * @param {string} msg
  * @return {object}
  */
  static badRequest(msg) {
    return new ApiError(400, msg);
  }
  /**
  * @param {string} msg
  * @return {object}
  */
  static internal(msg) {
    return new ApiError(500, msg);
  }
}

module.exports = ApiError;
