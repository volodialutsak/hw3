const {Schema, model} = require('mongoose');
const passwordHasher = require('../helpers/passwordHasher');
const {roles} = require('../constants/modelConstants');

const User = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },

  password: {
    type: String,
    required: true,
  },

  role: {
    type: String,
    enum: [...roles],
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now,
  },
});

// Hash password when register new user
User.post('validate', async function(user, next) {
  user.password = await passwordHasher(user.password);
  next();
});


module.exports = model('User', User);
