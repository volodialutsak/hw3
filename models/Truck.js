const {Schema, model} = require('mongoose');
const {truckStatuses, truckTypes} = require('../constants/modelConstants');

const Truck = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },

  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },

  status: {
    type: String,
    enum: [...truckStatuses],
    default: 'IS',
  },

  type: {
    type: String,
    enum: [...truckTypes],
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = model('Truck', Truck);
