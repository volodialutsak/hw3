const {Schema, model} = require('mongoose');
const {loadStatuses, loadStates} = require('../constants/modelConstants');


const Load = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },

  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },

  name: {
    type: String,
    required: true,
  },

  status: {
    type: String,
    enum: [...loadStatuses],
    default: loadStatuses[0],
  },

  state: {
    type: String,
    enum: [null, ...loadStates],
    default: null,
  },

  dimensions: {
    length: {
      type: Number,
      required: true,
    },
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },

  payload: {
    type: Number,
    required: true,
  },

  pickup_address: {
    type: String,
    required: true,
  },

  delivery_address: {
    type: String,
    required: true,
  },

  logs: [
    {
      message: {
        type: String,
        required: true,
      },
      time: {
        type: Date,
        default: Date.now,
        // required: true,
      },
    },
  ],

  createdDate: {
    type: Date,
    default: Date.now,
  },
});


module.exports = model('Load', Load);
