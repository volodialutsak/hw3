const Load = require('../models/Load');
const Truck = require('../models/Truck');
const ApiError = require('../middleware/utils/ApiError');
const {loadStates} = require('../constants/modelConstants');
const {log} = require('../helpers/loadLogger');
const checkDimensions = require('../helpers/checkDimensions');


const getLoads = async (user, status, offset, limit) => {
  if (user.role === 'DRIVER') {
    const filter = status ? {assigned_to: user._id, status} :
    {assigned_to: user._id};

    return await Load.find(filter)
        .skip(offset)
        .limit(limit)
        .select('-__v');
  }

  if (user.role === 'SHIPPER') {
    const filter = status ? {created_by: user._id, status} :
    {created_by: user._id};

    return await Load.find(filter)
        .skip(offset)
        .limit(limit)
        .select('-__v');
  }
};


const checkAndFormatLoad = (bodyData, shipperId) => {
  return new Load({
    created_by: shipperId,
    ...bodyData,
  });
};


const addLoad = async (load) => {
  return await Load.create(load);
};


const getActiveLoad = async (driverId) => {
  return await Load.findOne(
      {assigned_to: driverId, status: 'ASSIGNED'},
  ).select('-__v');
};


const nextLoadState = async (driverId) => {
  const load = await getActiveLoad(driverId);

  if (!load) {
    throw ApiError.badRequest(
        `Driver does not have an active load`,
    );
  }

  const state = await iterateLoadState(load);

  if (state === loadStates[3]) {
    await Load.findByIdAndUpdate(
        load._id,
        {$set: {status: 'SHIPPED'}},
    );

    log(
        load.id,
        `Load status changed from 'ASSIGNED'` +
        `to 'SHIPPED'`,
    );

    await Truck.findOneAndUpdate(
        {assigned_to: driverId},
        {$set: {status: 'IS'}},
    );
  }

  return state;
};


const getLoad = async (user, loadId) => {
  const load = await Load.findById(loadId).select('-__v');

  if (!load) {
    throw ApiError.badRequest(
        `Bad request. Cannot found load with id:${loadId}`,
    );
  }

  if (
    user.role === 'DRIVER' &&
    load.assigned_to?.toString() !== user._id.toString()
  ) {
    throw ApiError.badRequest(
        `Bad request. Driver is not assigned to the load with id:${loadId}`,
    );
  }

  if (
    user.role === 'SHIPPER' &&
    load.created_by.toString() !== user._id.toString()
  ) {
    throw ApiError.badRequest(
        `Bad request. Shipper did not created the load with id:${loadId}`,
    );
  }

  return load;
};

const updateLoadById = async (shipper, loadId, loadData) => {
  const load = await getLoad(shipper, loadId);
  checkLoadIsNew(load, 'update type of load');

  return await Load.findByIdAndUpdate(loadId, {$set: {...loadData}});
};

const updateLoad = async (shipper, loadId, loadData) => {
  await getLoad(shipper, loadId);
  return await Load.findByIdAndUpdate(loadId, {$set: {...loadData}});
};


const deleteLoad = async (shipper, loadId) => {
  const load = await getLoad(shipper, loadId);
  checkLoadIsNew(load, 'delete the load');

  return await Load.findByIdAndDelete(loadId);
};


const postLoad = async (shipper, loadId) => {
  const load = await getLoad(shipper, loadId);
  checkLoadIsNew(load, 'post the load for the shipper');

  await changeStatus(shipper, loadId, 'NEW', 'POSTED');

  let trucks = await Truck.find({status: 'IS'});

  trucks = trucks.filter((truck) => truck.assigned_to != null);

  if (trucks.length === 0) {
    await changeStatus(shipper, loadId,
        'POSTED', 'NEW', 'No free truck found.');
    return false;
  }

  trucks = trucks.filter((truck) => {
    return checkDimensions(truck.type, load.dimensions, load.payload);
  });

  if (trucks.length === 0) {
    await changeStatus(shipper, loadId, 'POSTED', 'NEW',
        'No free truck can carry load due to its payload or dimensions.');
    return false;
  }

  console.log(trucks);
  const truck = trucks[0];

  await Truck.findByIdAndUpdate(truck._id, {$set: {status: 'OL'}});
  await changeStatus(shipper, loadId, 'POSTED', 'ASSIGNED');
  await updateLoad(shipper, loadId,
      {state: loadStates[0], assigned_to: truck.assigned_to});

  log(load.id,
      `Load state changed to '${loadStates[0]}'`,
  );

  return true;
};


const getShippingInfo = async (shipper, loadId) => {
  const load = await getLoad(shipper, loadId);
  if (load.status !== 'ASSIGNED') {
    throw ApiError.badRequest(
        `Bad request. Load with id:${loadId} is not active`,
    );
  }

  const truck = await Truck.findOne({assigned_to: load.assigned_to});

  return {load, truck};
};


const checkLoadIsNew = (load, text) => {
  if (load.status !== 'NEW') {
    throw ApiError.badRequest(
        `Bad request. Cannot ${text}. Load was already posted.`,
    );
  }
};


const iterateLoadState = async (load) => {
  const index = loadStates.indexOf(load.state);
  const patchedLoad = await Load.findByIdAndUpdate(
      load._id,
      {$set: {state: loadStates[index + 1]}},
      {new: true},
  );

  log(
      load.id,
      `Load state changed from '${load.state}'` +
      `to '${patchedLoad.state}'`,
  );

  return patchedLoad.state;
};


const changeStatus = async (shipper, loadId, oldStatus, newStatus,
    message = '') => {
  await updateLoad(shipper, loadId, {status: newStatus});

  log(
      loadId,
      `${message} Load status changed from '${oldStatus}' to '${newStatus}'`,
  );
};


module.exports = {
  getLoads,
  checkAndFormatLoad,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoad,
  updateLoadById,
  deleteLoad,
  postLoad,
  getShippingInfo,
};
