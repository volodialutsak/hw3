const Truck = require('../models/Truck');
const ApiError = require('../middleware/utils/ApiError');

const getTrucks = async (driverId, offset, limit) => {
  return await Truck.find({created_by: driverId})
      .skip(offset)
      .limit(limit)
      .select('-__v');
};

const checkAndFormatTruck = (bodyData, driverId) => {
  const {type} = bodyData;
  return new Truck({
    created_by: driverId,
    type,
  });
};

const addTruck = async (truck) => {
  return await Truck.create(truck);
};

const getTruck = async (driverId, truckId) => {
  const truck = await Truck.findById(truckId).select('-__v');

  if (!truck) {
    throw ApiError.badRequest(`The Truck with id:${truckId} is not found`);
  }

  if ( truck.created_by.toString() !== driverId.toString() ) {
    throw ApiError.badRequest(`Unauthorized request. Bad token`);
  }

  return truck;
};

const updateTruck = async (driverId, truckId, type) => {
  const truck = await getTruck(driverId, truckId);

  checkTruckIsAssigned(truck, driverId, 'update');
  checkTruckIsOL(truck, 'update type of truck');

  return await Truck.findByIdAndUpdate(truckId, {$set: {type}});
};

const deleteTruck = async (driverId, truckId) => {
  const truck = await getTruck(driverId, truckId);

  checkTruckIsAssigned(truck, driverId, 'delete');
  checkTruckIsOL(truck, 'delete the truck');

  return await Truck.findByIdAndDelete(truckId);
};

const assignTruck = async (driverId, truckId) => {
  const truck = await getTruck(driverId, truckId);
  checkTruckIsOL(truck, 'assign the truck to the driver');

  const trucks = await getTrucks(driverId, 0, 0);

  trucks.filter((truck) => truck._id.toString() !== truckId.toString())
      .forEach((truck) => {
        if (truck.status === 'OL') {
          throw ApiError.badRequest(
              `Bad request. Cannot assign driver to other` +
              ` truck while his current truck is on load`,
          );
        }
      });

  trucks.forEach( async (item) => {
    if (item._id.toString() === truckId.toString()) {
      await Truck.findByIdAndUpdate(item._id, {$set: {assigned_to: driverId}});
    } else {
      await Truck.findByIdAndUpdate(item._id, {$set: {assigned_to: null}});
    }
  });
};

const checkTruckIsOL = (truck, text) => {
  if (truck.status === 'OL') {
    throw ApiError.badRequest(
        `Bad request. Cannot ${text} while it is on load`,
    );
  }
};

const checkTruckIsAssigned = (truck, driverId, text) => {
  if (truck.assigned_to?.toString() === driverId.toString()) {
    throw ApiError.badRequest(`The driver cannot ${text}` +
     `the truck assigned to him`);
  }
};

module.exports = {
  getTrucks,
  checkAndFormatTruck,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
