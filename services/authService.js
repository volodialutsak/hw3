const jwt = require(`jsonwebtoken`);
const bcrypt = require('bcrypt');
const User = require(`../models/User`);
const ApiError = require(`../middleware/utils/ApiError`);
const config = require('../config/config');

const checkAndFormatUserData = async (userData) => {
  const {email} = userData;

  // Check that username is unique
  const user = await User.findOne({email});
  if (user) {
    throw ApiError.badRequest(
        `User with email '${email}' already exists`,
    );
  }

  return new User(userData);
};

const createUser = async (user) => {
  return await User.create(user);
};

const getUser = async (email, password) => {
  const user = await User.findOne({email});
  if (!user) {
    throw ApiError.badRequest(`User with email '${email}' is not found`);
  }

  const result = await bcrypt.compare(password, user.password);
  if (!result) {
    throw ApiError.badRequest(`Wrong password`);
  }

  return user;
};

const generateToken = ({_id, email, password, role}) => {
  return jwt.sign({_id, email, password, role}, config.JWT_SECRET, {
    expiresIn: `30d`,
  });
};

module.exports = {
  checkAndFormatUserData,
  createUser,
  getUser,
  generateToken,
};
