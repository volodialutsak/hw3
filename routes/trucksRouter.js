const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const {driverAuth} = require('../middleware/authMiddleware');
const requestValidator = require('../middleware/requestValidator');

const {
  typeSchema,
} = require('../helpers/validators/truckSchemas');
const {
  queryParamsSchema,
  mongoIdSchema,
} = require('../helpers/validators/commonSchemas');
const {
  getTrucks,
  checkAndFormatTruck,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../services/trucksService');

// eslint-disable-next-line new-cap
const router = Router();

// @desc   Retrieve the list of trucks for a driver
// @route  GET /api/trucks
// @access Protected
router.get('/',
    driverAuth,
    requestValidator(queryParamsSchema, 'query'),
    asyncHandler(
        async (req, res) => {
          const {offset, limit} = req.query;
          const {_id: driverId} = req.user;
          const trucks = await getTrucks(driverId, offset, limit);

          res.status(200).json({trucks});
        },
    ),
);

// @desc   Add a truck for a driver
// @route  POST /api/trucks
// @access Protected
router.post('/',
    driverAuth,
    requestValidator(typeSchema, 'body'),
    asyncHandler(
        async (req, res) => {
          const {_id: driverId} = req.user;
          const truckDTO = checkAndFormatTruck(req.body, driverId);

          await addTruck(truckDTO);

          res.status(200).json({message: 'Truck created successfully'});
        },
    ),
);

// @desc   Get drivers's truck by id
// @route  GET /api/trucks/:id
// @access Protected
router.get('/:id',
    driverAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: truckId} = req.params;
          const {_id: driverId} = req.user;
          const truck = await getTruck(driverId, truckId);

          res.status(200).json({truck});
        },
    ),
);

// @desc   Update drivers's truck type by id
// @route  PUT /api/trucks/:id
// @access Protected
router.put('/:id',
    driverAuth,
    requestValidator(mongoIdSchema, 'params'),
    requestValidator(typeSchema, 'body'),
    asyncHandler(
        async (req, res) => {
          const {id: truckId} = req.params;
          const {type} = req.body;
          const {_id: driverId} = req.user;

          await updateTruck(driverId, truckId, type);

          res.status(200).json({message: 'Truck details changed successfully'});
        },
    ),
);

// @desc   Delete user's truck by id
// @route  DELETE /api/trucks/:id
// @access Protected
router.delete('/:id',
    driverAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: truckId} = req.params;
          const {_id: driverId} = req.user;

          await deleteTruck(driverId, truckId);

          res.status(200).json({message: 'Truck deleted successfully'});
        },
    ),
);

// @desc   Assign truck to driver by id
// @route  POST /api/trucks/:id/assign
// @access Protected
router.post('/:id/assign',
    driverAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: truckId} = req.params;
          const {_id: driverId} = req.user;

          await assignTruck(driverId, truckId);

          res.status(200).json({message: 'Truck assigned successfully'});
        },
    ),
);

module.exports = router;
