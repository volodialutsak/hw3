const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const {commonAuth} = require('../middleware/authMiddleware');
const requestValidator = require('../middleware/requestValidator');

const {passwordsSchema} = require('../helpers/validators/userSchemas');
const {
  // getUser,
  deleteUser,
  changePassword,
} = require('../services/userService');

// eslint-disable-next-line new-cap
const router = Router();

// @desc   User can request only his own profile info
// @route  GET /api/users/me
// @access Protected
router.get('/me',
    commonAuth,
    asyncHandler( async (req, res) => {
      const user = req.user;
      const {_id, role, email, createdDate} = user;

      res.status(200).json({user: {_id, role, email, createdDate}});
    }),
);

// @desc   User can delete only his own profile info
// @route  DELETE /api/users/me
// @access Protected
router.delete('/me',
    commonAuth,
    asyncHandler( async (req, res) => {
      const user = req.user;
      await deleteUser(user._id);

      res.status(200).json({message: 'Profile deleted successfully'});
    }),
);

// @desc   Change user's password
// @route  PATCH /api/users/me
// @access Protected
router.patch('/me/password',
    commonAuth,
    requestValidator(passwordsSchema, 'body'),
    asyncHandler( async (req, res) => {
      const user = req.user;
      const {newPassword} = req.body;
      await changePassword(user, newPassword);

      res.status(200).json({message: 'Password changed successfully'});
    }),
);

module.exports = router;
