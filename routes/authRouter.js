const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const requestValidator = require('../middleware/requestValidator');

const {
  credentialsSchema,
  userRegSchema,
} = require('../helpers/validators/userSchemas');
const {
  checkAndFormatUserData,
  createUser,
  generateToken,
  getUser,
} = require('../services/authService');


// eslint-disable-next-line new-cap
const router = Router();

// @desc   Create new profile
// @route  POST /api/auth/register
// @access Unprotected
router.post('/register',
    requestValidator(userRegSchema, 'body'),
    asyncHandler(async (req, res) => {
      const userDTO = await checkAndFormatUserData(req.body);
      await createUser(userDTO);

      res.status(200).json({message: 'Profile created successfully'});
    }),
);

// @desc   Login into the system, generate token
// @route  POST /api/auth/login
// @access Unprotected
router.post('/login',
    requestValidator(credentialsSchema, 'body'),
    asyncHandler(async (req, res) => {
      const {email, password, role} = req.body;
      const user = await getUser(email, password);
      const {_id} = user;
      const token = generateToken({_id, email, password, role});

      res.status(200).json({jwt_token: token});
    }),
);

module.exports = router;
