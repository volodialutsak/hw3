const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const {
  driverAuth,
  shipperAuth,
  commonAuth,
} = require('../middleware/authMiddleware');
const requestValidator = require('../middleware/requestValidator');

const {
  addLoadSchema,
  updateLoadSchema,
} = require('../helpers/validators/loadSchemas');
const {
  queryParamsSchema,
  mongoIdSchema,
} = require('../helpers/validators/commonSchemas');
const {
  getLoads,
  checkAndFormatLoad,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoad,
  updateLoadById,
  deleteLoad,
  postLoad,
  getShippingInfo,
} = require('../services/loadService');

// eslint-disable-next-line new-cap
const router = Router();

// @desc   Retrieve the list of loads for a user
// @route  GET /api/loads
// @access Protected
router.get('/',
    commonAuth,
    requestValidator(queryParamsSchema, 'query'),
    asyncHandler(
        async (req, res) => {
          const {status, offset, limit} = req.query;
          const loads = await getLoads(req.user, status, offset, limit);

          res.status(200).json({loads});
        },
    ),
);

// @desc   Add a load for a shipper
// @route  POST /api/loads
// @access Protected
router.post('/',
    shipperAuth,
    requestValidator(addLoadSchema, 'body'),
    asyncHandler(
        async (req, res) => {
          const {_id: shipperId} = req.user;
          const loadDTO = checkAndFormatLoad(req.body, shipperId);

          await addLoad(loadDTO);

          res.status(200).json({message: 'Load created successfully'});
        },
    ),
);

// @desc   Retrieve the active load for authorized driver
// @route  GET /api/loads/active
// @access Protected
router.get('/active',
    driverAuth,
    asyncHandler(
        async (req, res) => {
          const {_id: driverId} = req.user;
          const load = await getActiveLoad(driverId);

          res.status(200).json({load});
        },
    ),
);

// @desc   Iterate to next Load state
// @route  PATCH /api/loads/active/state
// @access Protected
router.patch('/active/state',
    driverAuth,
    asyncHandler(
        async (req, res) => {
          const {_id: driverId} = req.user;
          const state = await nextLoadState(driverId);

          res.status(200).json({message: `Load state changed to '${state}'`});
        },
    ),
);

// @desc   Get user's Load by id
// @route  GET /api/loads/:id
// @access Protected
router.get('/:id',
    commonAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: loadId} = req.params;
          const load = await getLoad(req.user, loadId);

          res.status(200).json({load});
        },
    ),
);

// @desc   Update shipper's load by id
// @route  PUT /api/loads/:id
// @access Protected
router.put('/:id',
    shipperAuth,
    requestValidator(mongoIdSchema, 'params'),
    requestValidator(updateLoadSchema, 'body'),
    asyncHandler(
        async (req, res) => {
          const {id: loadId} = req.params;
          await updateLoadById(req.user, loadId, req.body);

          res.status(200).json({message: `Load details changed successfully`});
        },
    ),
);

// @desc   Delete shipper's load by id
// @route  DELETE /api/loads/:id
// @access Protected
router.delete('/:id',
    shipperAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: loadId} = req.params;
          await deleteLoad(req.user, loadId);

          res.status(200).json({message: `Load deleted successfully`});
        },
    ),
);

// @desc   Post a user's load by id, search for drivers
// @route  POST /api/loads/{id}/post
// @access Protected
router.post('/:id/post',
    shipperAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: loadId} = req.params;

          const driverFound = await postLoad(req.user, loadId);

          res.status(200).json({
            message: 'Load posted successfully',
            driver_found: driverFound,
          });
        },
    ),
);

// @desc   Get user's Load shipping info by id,
//         returns detailed info about shipment for active loads
// @route  GET /api/loads/:id/shipping_info
// @access Protected
router.get('/:id/shipping_info',
    shipperAuth,
    requestValidator(mongoIdSchema, 'params'),
    asyncHandler(
        async (req, res) => {
          const {id: loadId} = req.params;
          const shippingInfo = await getShippingInfo(req.user, loadId);

          res.status(200).json(shippingInfo);
        },
    ),
);

module.exports = router;
