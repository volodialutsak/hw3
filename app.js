require('dotenv').config();
const logger = require('./middleware/utils/logger');
const express = require('express');
const morgan = require('morgan');
const ErrorHandler = require('./middleware/errorHandler');
const connectDB = require('./config/connectDB');
const cors = require('./middleware/cors');
const config = require('./config/config');
const ApiError = require('./middleware/utils/ApiError');

connectDB();

const app = express();

// CORS
app.use(cors);

// Parsers
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// Logging
app.use(morgan('dev'));

// Routes
app.use('/api/auth', require('./routes/authRouter'));
app.use('/api/users', require('./routes/usersRouter'));
app.use('/api/trucks', require('./routes/trucksRouter'));
app.use('/api/loads', require('./routes/loadsRouter'));
app.use('*', (req, res) => {
  throw ApiError.badRequest(`Bad request`);
});

// Error handler
app.use(ErrorHandler);

const port = config.PORT;

app.listen(port, (req, res) => {
  logger.info(`
    ################################################
          Server is listening on port: ${port}
    ################################################
  `);
}).on('error', (err) => {
  logger.error(err);
  process.exit(1);
});
